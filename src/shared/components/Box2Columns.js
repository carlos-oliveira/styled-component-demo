import styled from "styled-components";

const Box2Columns = styled.div`
  display: flex;
  flex-direction: row;
  justify-content: space-between;
  padding: 10px 0;
  border: ${props => props.border || "none"};
`;

export default Box2Columns;
