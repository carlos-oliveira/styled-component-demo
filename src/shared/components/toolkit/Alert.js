import React from "react";
import { TextLink } from "@nab/web-ui-toolkit";
import styled from "styled-components";

const MyTextLink = styled(TextLink)`
  cursor: pointer;
  text-decoration: underline;
  padding-bottom: 10px;
  color: green;
`;

const Alert = () => {
  return (
    <MyTextLink href="https://nab.com.au">Australia National Bank</MyTextLink>
  );
};

export default Alert;
