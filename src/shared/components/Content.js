import React from "react";
import styled from "styled-components";
import { theme } from "../../styles/global";

const ContentDiv = styled.div`
  display: flex;
  flex-direction: ${props => props.direction || ""};
  margin: 0 auto;
  justify-content: space-between;
  padding: 10px;
  border: 1px solid red;

  @media screen and (min-width: ${theme.screen.small}) 
    and (max-width: ${theme.screen.medium}) {    
      border 1px solid blue;
      padding: 10 20px;
      margin: 20px 10px;
    }

  @media screen  and (min-width: 1px) and (max-width: ${theme.screen.small}) {
    border 1px solid pink;
    padding: 5px 40px;
  }
`;

const Content = ({ children, direction }) => {
  return <ContentDiv direction={direction}>{children}</ContentDiv>;
};

export default Content;
