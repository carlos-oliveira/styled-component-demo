import React, { Component } from "react";
import Form from "./Form";
import Content from "./shared/components/Content";

class App extends Component {
  state = { text: "" };
  handleChange = e => {
    this.setState({ text: e.target.value });
  };

  render() {
    return (
      <div>
        <Content>
          <Form />
        </Content>
      </div>
    );
  }
}

export default App;
