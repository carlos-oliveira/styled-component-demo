import React from "react";
import styled from "styled-components";

import { Form, withFormik, Field } from "formik";
import * as Yup from "yup";
import Button from "./shared/components/Button";
import Box2Columns from "./shared/components/Box2Columns";
import Label from "./shared/components/Label";
import Alert from "./shared/components/toolkit/Alert";

const LocalWrapper = styled.div`
  margin: "20px";
`;

const LocalH1 = styled.h1`
  color: pink;
  border: 0.5px dotted;
`;

const LeftBox = styled.div`
  justify-content: flex-end;
`;

const ErrorDiv = styled.div`
  color: red;
  text-align: right;
`;

const StyledField = styled(Field)`
  color: green;
`;

const MyForm = ({
  values: { name, number },
  touched,
  errors,
  isSubmitting,
  isValid
}) => {
  return (
    <LocalWrapper>
      <LeftBox>
        <Alert />
      </LeftBox>
      <Form>
        <LocalH1>Form</LocalH1>
        {/* border="2px solid purple" */}
        <Box2Columns>
          <Label>name:</Label>
          <StyledField type="text" name="name" value={name} />
        </Box2Columns>
        <ErrorDiv>{errors.name && touched.name && errors.name}</ErrorDiv>
        <Box2Columns>
          <Label>number:</Label>
          <Field type="text" name="number" value={number} />
        </Box2Columns>
        <ErrorDiv>{errors.number && touched.number && errors.number}</ErrorDiv>

        <LeftBox>
          <Button
            margin="1rem 1rem 0 0"
            primary
            type="submit"
            disabled={!isValid || isSubmitting}
          >
            Submit
          </Button>
          <Button
            type="button"
            margin="1rem 0 1rem 1rem"
            onClick={() => alert("Cancel button")}
          >
            Cancel
          </Button>
        </LeftBox>
      </Form>
    </LocalWrapper>
  );
};

const MyFormikWhapper = withFormik({
  mapPropsToValues: () => ({ name: "", number: "" }),
  validationSchema: Yup.object().shape({
    name: Yup.string()
      .matches(/([a-z]+)$/, {
        message: "Incorrect format. Should be something like: FOO BAR",
        excludeEmptyString: true
      })
      .required("Required"),
    // number: Yup.string().matches(/([0-9]+)$/, {
    //   message: "Incorrect format. Example 123456",
    //   excludeEmptyString: true
    // }).required("Required")
    number: Yup.number()
      .typeError("Incorrect format your idiot. use: 123456")
      .max(10)
      .required("Required")
  }),
  handleSubmit({ name, number }) {
    console.log("Submitted:", name, number);
  }
})(MyForm);

export default MyFormikWhapper;
