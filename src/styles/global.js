export const theme = {
  screen: {
    xSmall: "300px",
    small: "400px",
    medium: "800px",
    large: "1024px"
  },
  font: {
    xSmall: { fontSize: 8 },
    small: { fontSize: 12 },
    medium: { fontSize: 14 },
    large: { fonteSize: 18 },
    xLarge: { fonteSize: 20 }
  }
};
