This project was bootstrapped with [Create React App](https://github.com/facebook/create-react-app).

## Available Scripts

In the project directory, you can run:

### `npm start`

Runs the app in the development mode.<br>
Open [http://localhost:3000](http://localhost:3000) to view it in the browser.

The page will reload if you make edits.<br>
You will also see any lint errors in the console.

## Styled component

- Take the name of this tool seriously becouse it is literally a way to style your component with Javascript.
- You are no longer styling HTML elements or components based on their class or HTML element:

```
<h1 className="title">Hello World</h1>

h1.title{
  font-size: 1.5em;
  color: purple;
}
```

- You’re defining styled components that possesses their own encapsulated styles:

```
import styled from 'styled-components';

const Title = styled.h1`
  font-size: 1.5em;
  color: purple;
`;

<Title>Hello World</Title>
```

- it utilises backtick syntax or Templete literal as oficially defined and introduced by ES6.

```
let name = `Carlos`;

//string interpolation
console.log(`Hi my name is ${name}`);
```

- From styled components docs: **It removes the mapping between components and styles. This means that when you're defining your styles, you're actually creating a normal React component, that has your styles attached to it.**
